package com.example.xsisacademy.picasso;


import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;

public class MainActivity extends AppCompatActivity {

    private ImageView iv;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        iv = (ImageView) findViewById(R.id.iv);

        Picasso.get()
                .load("https://www.sideshow.com/storage/product-images/903375/superman_dc-comics_silo_sm.png")
                .fit()
                .into(iv);

    }
}
